package io.cerberus.wallet.models.request;

import io.cerberus.wallet.db.models.AccountType;

public class AccountTypeDTO extends BaseDTO<AccountType> {
    public String ID;
    public String name;

    public AccountTypeDTO() {}

    public AccountTypeDTO(AccountType accountType) {
        ID = accountType.getExternalID();
        name = accountType.getName();
    }

    public boolean validate() {
        return true;
    }

    protected AccountType build() {
        return new AccountType(ID, name);
    }
}
