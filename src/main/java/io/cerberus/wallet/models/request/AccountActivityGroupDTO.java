package io.cerberus.wallet.models.request;

import io.cerberus.wallet.db.models.AccountActivityGroup;

public class AccountActivityGroupDTO extends BaseDTO<AccountActivityGroup> {
    public String ID;
    public AccountActivityGroupTypeDTO accountActivityGroupType;

    public AccountActivityGroupDTO() {}

    public AccountActivityGroupDTO(AccountActivityGroup accountActivityGroup) {
        ID = accountActivityGroup.getExternalID();
        accountActivityGroupType = new AccountActivityGroupTypeDTO(accountActivityGroup.getAccountActivityGroupType());
    }

    public boolean validate() {
        return true;
    }

    protected AccountActivityGroup build() {
        return new AccountActivityGroup(ID, accountActivityGroupType.ID);
    }
}
