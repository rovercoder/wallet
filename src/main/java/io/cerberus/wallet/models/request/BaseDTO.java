package io.cerberus.wallet.models.request;

import java.io.Serializable;

public abstract class BaseDTO<T> implements Serializable {

    public BaseDTO() {}

    public BaseDTO(T user) {}

    public abstract boolean validate();

    protected abstract T build();

    public T get() {
        return validate() ? build() : null;
    }
}
