package io.cerberus.wallet.models.request;

import io.cerberus.wallet.db.models.Currency;

public class CurrencyDTO extends BaseDTO<Currency> {

    public String ID;
    public String code;
    public String symbol;
    public Integer decimals;

    public CurrencyDTO() {}

    public CurrencyDTO(Currency currency) {
        ID = currency.getExternalID();
        code = currency.getCode();
        symbol = currency.getSymbol();
        decimals = currency.getDecimals();
    }

    @Override
    public boolean validate() {
        return true;
    }

    @Override
    protected Currency build() {
        return new Currency(ID, code, null, symbol, decimals);
    }
}
