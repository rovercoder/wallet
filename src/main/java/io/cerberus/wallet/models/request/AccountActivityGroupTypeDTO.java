package io.cerberus.wallet.models.request;

import io.cerberus.wallet.db.models.AccountActivityGroupType;

public class AccountActivityGroupTypeDTO extends BaseDTO<AccountActivityGroupType> {
    public String ID;
    public String name;

    public AccountActivityGroupTypeDTO() {}

    public AccountActivityGroupTypeDTO(AccountActivityGroupType accountActivityGroupType) {
        ID = accountActivityGroupType.getExternalID();
        name = accountActivityGroupType.getName();
    }

    public boolean validate() {
        return true;
    }

    protected AccountActivityGroupType build() {
        return new AccountActivityGroupType(ID, name);
    }
}
