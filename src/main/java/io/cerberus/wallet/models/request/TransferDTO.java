package io.cerberus.wallet.models.request;

import io.cerberus.wallet.db.models.AccountActivity;

import java.math.BigDecimal;
import java.util.Date;

public class TransferDTO extends BaseDTO<AccountActivity> {

    public String accountActivityID;
    public BigDecimal amount;
    public Date date;

    public AccountDTO fromAccount;
    public AccountDTO toAccount;

    @Override
    public boolean validate() {
        return false;
    }

    @Override
    protected AccountActivity build() {
        return null;
    }
}
