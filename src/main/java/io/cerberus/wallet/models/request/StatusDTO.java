package io.cerberus.wallet.models.request;

import io.cerberus.wallet.db.models.Status;

public class StatusDTO extends BaseDTO<Status> {
    public String ID;
    public String name;

    public StatusDTO() {}

    public StatusDTO(Status status) {
        ID = status.getExternalID();
        name = status.getName();
    }

    public boolean validate() {
        return true;
    }

    protected Status build() {
        return new Status(ID, name);
    }
}
