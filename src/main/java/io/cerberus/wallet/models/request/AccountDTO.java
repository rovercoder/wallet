package io.cerberus.wallet.models.request;

import io.cerberus.wallet.db.models.Account;
import io.cerberus.wallet.db.models.AccountType;
import io.cerberus.wallet.helpers.Enum;

import java.math.BigDecimal;

public class AccountDTO extends BaseDTO<Account> {

    public String ID;
    public AccountTypeDTO type;
    public BigDecimal amountBalance;
    public String name;
    public UserDTO user;
    public CurrencyDTO currency;
    public StatusDTO status;

    public AccountDTO() {}

    public AccountDTO(Account account) {
        ID = account.getExternalID();
        type = new AccountTypeDTO(account.getAccountType());
        amountBalance = account.getAmountBalance();
        name = account.getName();
        user = new UserDTO(account.getUser());
        currency = new CurrencyDTO(account.getCurrency());
        status = new StatusDTO(account.getStatus());
    }

    public boolean validate() {
        return true;
    }

    protected Account build() {
        return new Account(ID, type.ID, amountBalance, name, user.ID, currency.ID, status.ID);
    }
}
