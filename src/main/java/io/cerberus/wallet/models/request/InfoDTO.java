package io.cerberus.wallet.models.request;

import io.cerberus.wallet.Context;
import io.cerberus.wallet.db.models.Session;

public class InfoDTO extends BaseDTO<Session> {
    public String sessionID;
    public UserDTO user;

    public InfoDTO() {
        Session session = Context.getInstance().session;
        this.sessionID = session.getExternalID();
        this.user = new UserDTO(session.getUser());
    }

    @Override
    public boolean validate() {
        return false;
    }

    @Override
    protected Session build() {
        return null;
    }
}
