package io.cerberus.wallet.models.request;

import io.cerberus.wallet.db.models.User;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class UserDTO extends BaseDTO<User> {
    public String ID;
    public String name;
    public String surname;
    public String email;
    public UserRoleDTO role;
    public StatusDTO status;
    public List<AccountDTO> accounts;

    public UserDTO() {}

    public UserDTO(User user) {
        ID = user.getExternalID();
        name = user.getName();
        surname = user.getSurname();
        email = user.getEmail();
        role = new UserRoleDTO(user.getRole());
        status = new StatusDTO(user.getStatus());
        accounts = user.getAccounts().stream().map(AccountDTO::new).collect(Collectors.toList());
    }

    public boolean validate() {
        //TODO: Validation
        return true;
    }

    protected User build() {
        return new User(ID, name, surname, email, role.ID, status.ID);
    }
}
