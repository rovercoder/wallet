package io.cerberus.wallet.models.request;

import io.cerberus.wallet.db.models.Account;

import java.math.BigDecimal;

public class AccountBalanceDTO extends BaseDTO<Account> {

    public String accountID;
    public CurrencyDTO currency;
    public BigDecimal amountBalance;

    public AccountBalanceDTO() {}

    public AccountBalanceDTO(Account account) {
        accountID = account.getExternalID();
        currency = new CurrencyDTO(account.getCurrency());
        amountBalance = account.getAmountBalance();
    }

    @Override
    public boolean validate() {
        return true;
    }

    @Override //removed props as should be one way only
    protected Account build() {
        return new Account(accountID);
    }
}
