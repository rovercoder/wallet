package io.cerberus.wallet.models.request;

import io.cerberus.wallet.db.models.UserRole;

public class UserRoleDTO extends BaseDTO<UserRole> {
    public String ID;
    public String name;

    public UserRoleDTO() {}

    public UserRoleDTO(UserRole userRole) {
        ID = userRole.getExternalID();
        name = userRole.getName();
    }

    public boolean validate() {
        return true;
    }

    protected UserRole build() {
        return new UserRole(ID, name);
    }
}
