package io.cerberus.wallet.models.request;

import io.cerberus.wallet.db.models.Session;
import io.cerberus.wallet.helpers.Common.string;

public class LoginDTO extends BaseDTO<Session> {
    public String email;
    public String password;

    public LoginDTO() {}

    @Override
    public boolean validate() {
        return !(string.isNullOrWhiteSpace(this.email) || string.isNullOrWhiteSpace(this.password));
    }

    @Override
    protected Session build() {
        return null;
    }
}
