package io.cerberus.wallet.models.request;

import io.cerberus.wallet.db.models.AccountActivityType;

public class AccountActivityTypeDTO extends BaseDTO<AccountActivityType> {
    public String ID;
    public String name;

    public AccountActivityTypeDTO() {}

    public AccountActivityTypeDTO(AccountActivityType accountActivityType) {
        ID = accountActivityType.getExternalID();
        name = accountActivityType.getName();
    }

    public boolean validate() {
        return true;
    }

    protected AccountActivityType build() {
        return new AccountActivityType(ID, name);
    }
}
