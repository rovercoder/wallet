package io.cerberus.wallet.models.request;

import io.cerberus.wallet.db.models.AccountActivity;

import java.math.BigDecimal;
import java.util.Date;

public class AccountActivityDTO extends BaseDTO<AccountActivity> {

    public String ID;
    public BigDecimal amount;
    public Date date;

    public AccountDTO fromAccount;
    public AccountDTO toAccount;

    public AccountActivityGroupDTO accountActivityGroupDTO;
    public AccountActivityTypeDTO accountActivityTypeDTO;

    public AccountActivityDTO() {}

    public AccountActivityDTO(AccountActivity accountActivity) {
        ID = accountActivity.getExternalID();
        amount = accountActivity.getAmount();
        date = accountActivity.getDate();
        fromAccount = new AccountDTO(accountActivity.getFromAccount());
        toAccount = new AccountDTO(accountActivity.getToAccount());
        accountActivityGroupDTO = new AccountActivityGroupDTO(accountActivity.getAccountActivityGroup());
        accountActivityTypeDTO = new AccountActivityTypeDTO(accountActivity.getAccountActivityType());
    }

    @Override
    public boolean validate() {
        return false;
    }

    @Override
    protected AccountActivity build() {
        return new AccountActivity(ID);
    }
}
