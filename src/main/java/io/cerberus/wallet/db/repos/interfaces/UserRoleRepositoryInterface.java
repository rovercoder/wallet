package io.cerberus.wallet.db.repos.interfaces;

import io.cerberus.wallet.db.models.UserRole;
import org.springframework.data.repository.CrudRepository;

public interface UserRoleRepositoryInterface extends CrudRepository<UserRole, Integer> {

}