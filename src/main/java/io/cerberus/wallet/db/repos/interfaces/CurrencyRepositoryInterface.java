package io.cerberus.wallet.db.repos.interfaces;

import io.cerberus.wallet.db.models.Currency;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CurrencyRepositoryInterface extends CrudRepository<Currency, Integer> {
    Currency findFirstByCode(String code);

    List<Currency> findAllByCodeIn(List<String> codes);
}