package io.cerberus.wallet.db.repos;


import io.cerberus.wallet.db.models.AccountType;
import io.cerberus.wallet.db.repos.interfaces.AccountTypeRepositoryInterface;

public abstract class AccountTypeRepository extends BaseRepository<AccountType, Integer> implements AccountTypeRepositoryInterface {
    public AccountTypeRepository() {
        super(AccountType.cryptoDomainID);
    }

}