package io.cerberus.wallet.db.repos;

import io.cerberus.wallet.db.models.AccountActivityGroupType;
import io.cerberus.wallet.db.repos.interfaces.AccountActivityGroupTypeRepositoryInterface;

public abstract class AccountActivityGroupTypeRepository extends BaseRepository<AccountActivityGroupType, Integer> implements AccountActivityGroupTypeRepositoryInterface {
    public AccountActivityGroupTypeRepository() {
        super(AccountActivityGroupType.cryptoDomainID);
    }

}