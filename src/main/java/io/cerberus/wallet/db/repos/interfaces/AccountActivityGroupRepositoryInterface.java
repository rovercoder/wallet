package io.cerberus.wallet.db.repos.interfaces;

import io.cerberus.wallet.db.models.AccountActivityGroup;
import org.springframework.data.repository.CrudRepository;

public interface AccountActivityGroupRepositoryInterface extends CrudRepository<AccountActivityGroup, Long> {

}