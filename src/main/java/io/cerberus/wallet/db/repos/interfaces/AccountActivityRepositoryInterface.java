package io.cerberus.wallet.db.repos.interfaces;

import io.cerberus.wallet.db.models.AccountActivity;
import org.springframework.data.repository.CrudRepository;

public interface AccountActivityRepositoryInterface extends CrudRepository<AccountActivity, Long> {

}