package io.cerberus.wallet.db.repos;

import io.cerberus.wallet.db.models.AccountActivityGroup;
import io.cerberus.wallet.db.repos.interfaces.AccountActivityGroupRepositoryInterface;

public abstract class AccountActivityGroupRepository extends BaseRepository<AccountActivityGroup, Long> implements AccountActivityGroupRepositoryInterface {
    public AccountActivityGroupRepository() {
        super(AccountActivityGroup.cryptoDomainID);
    }

}