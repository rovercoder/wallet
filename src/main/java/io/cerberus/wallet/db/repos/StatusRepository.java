package io.cerberus.wallet.db.repos;

import io.cerberus.wallet.db.models.Status;
import io.cerberus.wallet.db.repos.interfaces.StatusRepositoryInterface;

public abstract class StatusRepository extends BaseRepository<Status, Integer> implements StatusRepositoryInterface {
    public StatusRepository() {
        super(Status.cryptoDomainID);
    }

}