package io.cerberus.wallet.db.repos;

import io.cerberus.wallet.db.models.Account;
import io.cerberus.wallet.db.repos.interfaces.AccountRepositoryInterface;

public abstract class AccountRepository extends BaseRepository<Account, Long> implements AccountRepositoryInterface {
    public AccountRepository() {
        super(Account.cryptoDomainID);
    }
}