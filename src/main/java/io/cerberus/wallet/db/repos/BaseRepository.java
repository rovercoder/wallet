package io.cerberus.wallet.db.repos;

import io.cerberus.wallet.db.repos.interfaces.BaseRepositoryInterface;
import io.cerberus.wallet.helpers.Common.string;
import io.cerberus.wallet.helpers.Common.field;
import io.cerberus.wallet.helpers.Enum;

import java.util.Optional;

public abstract class BaseRepository<T, Z> implements BaseRepositoryInterface<T, Z> {

    private Enum.CryptoDomain cryptoDomain;

    public BaseRepository(Enum.CryptoDomain cryptoDomain) {
        this.cryptoDomain = cryptoDomain;
    }

    public T findById(String id) {
        Optional<T> optional = findById((Z)cryptoDomain.decrypt(id));
        if (optional.isEmpty())
            return null;
        return optional.get();
    }

    public T validateAndFindById(String id, String fieldName) {
        if (string.isNullOrWhiteSpace(id))
            field.throwInvalid(fieldName);

        id = id.trim();
        T object = findById(id);
        if (object == null)
            field.throwInvalid(fieldName);

        return object;
    }

    public T validateAndFindById(Z id, String fieldName) {
        if (id == null)
            field.throwInvalid(fieldName);

        Optional<T> optional = findById(id);
        if (optional.isEmpty())
            field.throwInvalid(fieldName);

        return optional.get();
    }
}
