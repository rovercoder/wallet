package io.cerberus.wallet.db.repos.interfaces;

import io.cerberus.wallet.db.models.AccountActivityGroupType;
import org.springframework.data.repository.CrudRepository;

public interface AccountActivityGroupTypeRepositoryInterface extends CrudRepository<AccountActivityGroupType, Integer> {

}