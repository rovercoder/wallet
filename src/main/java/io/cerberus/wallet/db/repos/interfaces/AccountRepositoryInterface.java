package io.cerberus.wallet.db.repos.interfaces;

import io.cerberus.wallet.db.models.Account;
import io.cerberus.wallet.db.models.AccountType;
import io.cerberus.wallet.db.models.Currency;
import io.cerberus.wallet.db.models.User;
import io.cerberus.wallet.db.models.Status;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AccountRepositoryInterface extends CrudRepository<Account, Long> {
    Account findFirstByUserAndCurrencyAndAccountTypeAndStatus(User user, Currency currency, AccountType accountType, Status status);
    List<Account> findAllByUser(User user);
}