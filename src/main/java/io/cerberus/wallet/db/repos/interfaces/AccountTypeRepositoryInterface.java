package io.cerberus.wallet.db.repos.interfaces;

import io.cerberus.wallet.db.models.AccountType;
import org.springframework.data.repository.CrudRepository;

public interface AccountTypeRepositoryInterface extends CrudRepository<AccountType, Integer> {

}