package io.cerberus.wallet.db.repos;

import io.cerberus.wallet.db.models.Session;
import io.cerberus.wallet.db.repos.interfaces.SessionRepositoryInterface;

public abstract class SessionRepository extends BaseRepository<Session, Long> implements SessionRepositoryInterface {
    public SessionRepository() {
        super(Session.cryptoDomainID);
    }

}