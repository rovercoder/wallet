package io.cerberus.wallet.db.repos.interfaces;

import io.cerberus.wallet.db.models.Status;
import org.springframework.data.repository.CrudRepository;

public interface StatusRepositoryInterface extends CrudRepository<Status, Integer> {

}