package io.cerberus.wallet.db.repos.interfaces;

import io.cerberus.wallet.db.models.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepositoryInterface extends CrudRepository<User, Long> {
    User findFirstByEmailEquals(String email);
}