package io.cerberus.wallet.db.repos;

import io.cerberus.wallet.db.models.UserRole;
import io.cerberus.wallet.db.repos.interfaces.UserRoleRepositoryInterface;

public abstract class UserRoleRepository extends BaseRepository<UserRole, Integer> implements UserRoleRepositoryInterface {
    public UserRoleRepository() {
        super(UserRole.cryptoDomainID);
    }

}