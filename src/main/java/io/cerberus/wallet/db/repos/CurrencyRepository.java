package io.cerberus.wallet.db.repos;

import io.cerberus.wallet.db.models.Currency;
import io.cerberus.wallet.db.repos.interfaces.CurrencyRepositoryInterface;

public abstract class CurrencyRepository extends BaseRepository<Currency, Integer> implements CurrencyRepositoryInterface {
    public CurrencyRepository() {
        super(Currency.cryptoDomainID);
    }

}