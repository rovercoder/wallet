package io.cerberus.wallet.db.repos;

import io.cerberus.wallet.db.models.User;
import io.cerberus.wallet.db.repos.interfaces.UserRepositoryInterface;

public abstract class UserRepository extends BaseRepository<User, Long> implements UserRepositoryInterface {
    public UserRepository() {
        super(User.cryptoDomainID);
    }
}