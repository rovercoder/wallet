package io.cerberus.wallet.db.repos.interfaces;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface BaseRepositoryInterface<T, Z> extends CrudRepository<T, Z> {

    T findById(String id);

    T validateAndFindById(String id, String fieldName);

    T validateAndFindById(Z id, String fieldName);
}
