package io.cerberus.wallet.db.repos.interfaces;

import io.cerberus.wallet.db.models.AccountActivityType;
import org.springframework.data.repository.CrudRepository;

public interface AccountActivityTypeRepositoryInterface extends CrudRepository<AccountActivityType, Integer> {

}