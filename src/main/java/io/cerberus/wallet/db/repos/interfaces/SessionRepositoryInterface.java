package io.cerberus.wallet.db.repos.interfaces;

import io.cerberus.wallet.db.models.Session;
import org.springframework.data.repository.CrudRepository;

public interface SessionRepositoryInterface extends CrudRepository<Session, Long> {

}