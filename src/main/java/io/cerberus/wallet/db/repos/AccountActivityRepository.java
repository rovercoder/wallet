package io.cerberus.wallet.db.repos;

import io.cerberus.wallet.db.models.AccountActivity;
import io.cerberus.wallet.db.repos.interfaces.AccountActivityRepositoryInterface;

public abstract class AccountActivityRepository extends BaseRepository<AccountActivity, Long> implements AccountActivityRepositoryInterface {
    public AccountActivityRepository() {
        super(AccountActivity.cryptoDomainID);
    }

}