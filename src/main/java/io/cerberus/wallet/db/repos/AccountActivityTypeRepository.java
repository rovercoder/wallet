package io.cerberus.wallet.db.repos;

import io.cerberus.wallet.db.models.AccountActivityType;
import io.cerberus.wallet.db.repos.interfaces.AccountActivityTypeRepositoryInterface;

public abstract class AccountActivityTypeRepository extends BaseRepository<AccountActivityType, Integer> implements AccountActivityTypeRepositoryInterface {
    public AccountActivityTypeRepository() {
        super(AccountActivityType.cryptoDomainID);
    }

}