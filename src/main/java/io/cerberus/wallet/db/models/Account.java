package io.cerberus.wallet.db.models;

import io.cerberus.wallet.helpers.Enum;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.CascadeType;
import javax.persistence.Transient;
import java.math.BigDecimal;
import java.security.InvalidParameterException;
import java.util.List;
import java.util.Objects;

@Entity
public class Account {
    @Transient public static Enum.CryptoDomain cryptoDomainID = Enum.CryptoDomain.AccountID;

    @Id @GeneratedValue private Long ID;
    private BigDecimal amountBalance;
    private String name;

    @ManyToOne(fetch = FetchType.EAGER) @JoinColumn(name = "account_type_id") private AccountType accountType;
    @ManyToOne(fetch = FetchType.EAGER) @JoinColumn(name = "user_id") private User user;
    @ManyToOne(fetch = FetchType.EAGER) @JoinColumn(name = "currency_id") private Currency currency;
    @ManyToOne(fetch = FetchType.EAGER) @JoinColumn(name = "status_id") private Status status;

    @OneToMany(mappedBy = "fromAccount", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER) private List<AccountActivity> fromAccountActivities;
    @OneToMany(mappedBy = "toAccount", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER) private List<AccountActivity> toAccountActivities;

    public Account(Long ID) {
        this.ID = ID;
    }

    public Account(String ID) {
        this(getID(ID));
    }

    public Account(Long ID, Integer accountTypeID, BigDecimal amountBalance, String name, Long userID, Integer currencyID, Integer statusID) {
        this.ID = ID;
        this.accountType = new AccountType(accountTypeID);
        this.amountBalance = amountBalance;
        this.name = name;
        this.user = new User(userID);
        this.currency = new Currency(currencyID);
        this.status = new Status(statusID);
    }

    public Account(String ID, String accountTypeID, BigDecimal amountBalance, String name, String userID, String currencyID, String statusID) {
        this(getID(ID), AccountType.getID(accountTypeID), amountBalance, name, User.getID(userID), Currency.getID(currencyID), Status.getID(statusID));
    }

    public Long getID() {
        return ID;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }

    public AccountType getAccountType() {
        return accountType;
    }

    public void setAccountType(AccountType accountType) {
        this.accountType = accountType;
    }

    public BigDecimal getAmountBalance() {
        return amountBalance;
    }

    public void setAmountBalance(BigDecimal amountBalance) {
        this.amountBalance = amountBalance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public List<AccountActivity> getFromAccountActivities() {
        return fromAccountActivities;
    }

    public void setFromAccountActivities(List<AccountActivity> fromAccountActivities) {
        this.fromAccountActivities = fromAccountActivities;
    }

    public List<AccountActivity> getToAccountActivities() {
        return toAccountActivities;
    }

    public void setToAccountActivities(List<AccountActivity> toAccountActivities) {
        this.toAccountActivities = toAccountActivities;
    }

    /* ****** ?? ****** *//* ****** ?? ****** *//* ****** ?? ****** *//* ****** ?? ****** */
    /*                               RELATIONSHIP ADDITIONAL                              */
    /* ****** ?? ****** *//* ****** ?? ****** *//* ****** ?? ****** *//* ****** ?? ****** */

    public void addFromAccountActivity(AccountActivity fromAccountActivity) {
        fromAccountActivities.add(fromAccountActivity);
        fromAccountActivity.setFromAccount(this);
    }

    public void removeFromAccountActivity(AccountActivity fromAccountActivity) {
        fromAccountActivities.remove(fromAccountActivity);
        fromAccountActivity.setFromAccount(null);
    }

    public void addToAccountActivity(AccountActivity toAccountActivity) {
        toAccountActivities.add(toAccountActivity);
        toAccountActivity.setToAccount(this);
    }

    public void removeToAccountActivity(AccountActivity toAccountActivity) {
        toAccountActivities.remove(toAccountActivity);
        toAccountActivity.setToAccount(null);
    }

    public Account addAmountBalance(BigDecimal amountToAdd) {
        this.amountBalance = this.amountBalance.add(amountToAdd);
        return this;
    }

    public Account remAmountBalance(BigDecimal amountToSubtract) {
        this.amountBalance = this.amountBalance.subtract(amountToSubtract);
        return this;
    }

    public boolean ensureAmountBalance(BigDecimal amountToHave) {
        return this.amountBalance.subtract(amountToHave).signum() >= 0;
    }

    public boolean validAmountBalance() {
        return this.amountBalance.signum() >= 0;
    }

    public Account validateAmountBalance(boolean skip) {
        if (!skip && !validAmountBalance())
            throw new InvalidParameterException("Invalid account balance!");
        return this;
    }
    public Account validateAmountBalance() {
        return validateAmountBalance(false);
    }

    public String getExternalID() {
        return cryptoDomainID.encrypt(ID);
    }

    public void setExternalID(String ID) {
        this.ID = getID(ID);
    }

    public static Long getID(String ID) {
        return cryptoDomainID.decryptLong(ID);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Account)) return false;
        return ID != null && ID.equals(((Account) o).ID);
    }
    @Override
    public int hashCode() {
        return Objects.hash(getID());
    }
}
