package io.cerberus.wallet.db.models;

import io.cerberus.wallet.helpers.Enum;

import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.Transient;
import java.math.BigDecimal;
import java.util.Date;
import java.time.Instant;
import java.util.Objects;

@Entity
public class AccountActivity {
    @Transient public static Enum.CryptoDomain cryptoDomainID = Enum.CryptoDomain.AccountActivityID;

    @Id @GeneratedValue private Long ID;
    private BigDecimal amount;
    private Date date = Date.from(Instant.now());

    @ManyToOne(fetch = FetchType.EAGER) @JoinColumn(name = "account_activity_group_id") private AccountActivityGroup accountActivityGroup;
    @ManyToOne(fetch = FetchType.EAGER) @JoinColumn(name = "account_activity_type_id") private AccountActivityType accountActivityType;

    @ManyToOne(fetch = FetchType.EAGER) @JoinColumn(name = "from_account_id", referencedColumnName = "id") private Account fromAccount;
    @ManyToOne(fetch = FetchType.EAGER) @JoinColumn(name = "to_account_id", referencedColumnName = "id") private Account toAccount;

    public AccountActivity(Long ID) {
        this.ID = ID;
    }

    public AccountActivity(String ID) {
        this(getID(ID));
    }

    public AccountActivity(Long ID, BigDecimal amount, Long accountActivityGroupID, Integer accountActivityTypeID, Long fromAccountID, Long toAccountID) {
        this.ID = ID;
        this.amount = amount;
        this.accountActivityGroup = new AccountActivityGroup(accountActivityGroupID);
        this.accountActivityType = new AccountActivityType(accountActivityTypeID);
        this.fromAccount = new Account(fromAccountID);
        this.toAccount = new Account(toAccountID);
    }

    public AccountActivity(String ID, BigDecimal amount, String accountActivityGroupID, String accountActivityTypeID, String fromAccountID, String toAccountID) {
        this(getID(ID), amount, AccountActivityGroup.getID(accountActivityGroupID), AccountActivityType.getID(accountActivityTypeID), Account.getID(fromAccountID), Account.getID(toAccountID));
    }

    public Long getID() {
        return ID;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public AccountActivityGroup getAccountActivityGroup() {
        return accountActivityGroup;
    }

    public void setAccountActivityGroup(AccountActivityGroup accountActivityGroup) {
        this.accountActivityGroup = accountActivityGroup;
    }

    public AccountActivityType getAccountActivityType() {
        return accountActivityType;
    }

    public void setAccountActivityType(AccountActivityType accountActivityType) {
        this.accountActivityType = accountActivityType;
    }

    public Account getFromAccount() {
        return fromAccount;
    }

    public void setFromAccount(Account fromAccount) {
        this.fromAccount = fromAccount;
    }

    public Account getToAccount() {
        return toAccount;
    }

    public void setToAccount(Account toAccount) {
        this.toAccount = toAccount;
    }

    public String getExternalID() {
        return cryptoDomainID.encrypt(ID);
    }

    public void setExternalID(String ID) {
        this.ID = getID(ID);
    }

    public static Long getID(String ID) {
        return cryptoDomainID.decryptLong(ID);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AccountActivity)) return false;
        return ID != null && ID.equals(((AccountActivity) o).ID);
    }
    @Override
    public int hashCode() {
        return Objects.hash(getID());
    }
}
