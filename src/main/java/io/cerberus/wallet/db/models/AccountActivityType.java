package io.cerberus.wallet.db.models;

import io.cerberus.wallet.helpers.Enum;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.FetchType;
import javax.persistence.CascadeType;
import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
public class AccountActivityType {
    @Transient public static Enum.CryptoDomain cryptoDomainID = Enum.CryptoDomain.AccountActivityTypeID;

    @Id private Integer ID;
    private String name;

    @OneToMany(mappedBy = "accountActivityType", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER) private List<AccountActivity> accountActivities = new ArrayList<>();

    public AccountActivityType(Integer ID) {
        this.ID = ID;
    }

    public AccountActivityType(String ID) {
        this(getID(ID));
    }

    public AccountActivityType(Integer ID, String name) {
        this.ID = ID;
        this.name = name;
    }

    public AccountActivityType(String ID, String name) {
        this(getID(ID), name);
    }

    public Integer getID() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<AccountActivity> getAccountActivities() {
        return accountActivities;
    }

    public void setAccountActivities(List<AccountActivity> accountActivities) {
        this.accountActivities = accountActivities;
    }

    /* ****** ?? ****** *//* ****** ?? ****** *//* ****** ?? ****** *//* ****** ?? ****** */
    /*                               RELATIONSHIP ADDITIONAL                              */
    /* ****** ?? ****** *//* ****** ?? ****** *//* ****** ?? ****** *//* ****** ?? ****** */

    public void addAccountActivity(AccountActivity accountActivity) {
        accountActivities.add(accountActivity);
        accountActivity.setAccountActivityType(this);
    }

    public void removeAccountActivity(AccountActivity accountActivity) {
        accountActivities.remove(accountActivity);
        accountActivity.setAccountActivityType(null);
    }

    public String getExternalID() {
        return cryptoDomainID.encrypt(ID);
    }

    public void setExternalID(String ID) {
        this.ID = getID(ID);
    }

    public static Integer getID(String ID) {
        return cryptoDomainID.decryptInteger(ID);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AccountActivityType)) return false;
        return ID != null && ID.equals(((AccountActivityType) o).ID);
    }
    @Override
    public int hashCode() {
        return Objects.hash(getID());
    }
}
