package io.cerberus.wallet.db.models;

import io.cerberus.wallet.helpers.Enum;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.ManyToOne;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.CascadeType;
import javax.persistence.Transient;
import java.util.List;
import java.util.ArrayList;
import java.util.Objects;

@Entity
public class User {
    @Transient public static Enum.CryptoDomain cryptoDomainID = Enum.CryptoDomain.UserID;

    @Id @GeneratedValue private Long ID;
    private String name;
    private String surname;
    private String email;

    @ManyToOne(fetch = FetchType.EAGER) @JoinColumn(name = "role_id") private UserRole role;
    @ManyToOne(fetch = FetchType.EAGER) @JoinColumn(name = "status_id") private Status status;
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER) private List<Account> accounts = new ArrayList<>();
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER) private List<Session> sessions = new ArrayList<>();

    //TODO: details object (containing e.g. address)

    public User(Long ID) {
        this.ID = ID;
    }

    public User(String ID) {
        this(getID(ID));
    }

    public User(Long ID, String name, String surname, String email, Integer roleID, Integer statusID) {
        this.ID = ID;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.role = Enum.UserRole.Normie.unless(roleID).construct();
        this.status = Enum.Status.Active.unless(statusID).construct();
    }

    public User(String ID, String name, String surname, String email, String roleID, String statusID) {
        this(getID(ID), name, surname, email, UserRole.getID(roleID), Status.getID(statusID));
    }

    public Long getID() {
        return ID;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public List<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<Account> accounts) {
        this.accounts = accounts;
    }

    public List<Session> getSessions() {
        return sessions;
    }

    public void setSessions(List<Session> sessions) {
        this.sessions = sessions;
    }

    /* ****** ?? ****** *//* ****** ?? ****** *//* ****** ?? ****** *//* ****** ?? ****** */
    /*                               RELATIONSHIP ADDITIONAL                              */
    /* ****** ?? ****** *//* ****** ?? ****** *//* ****** ?? ****** *//* ****** ?? ****** */

    public void addAccount(Account account) {
        accounts.add(account);
        account.setUser(this);
    }

    public void removeAccount(Account account) {
        accounts.remove(account);
        account.setUser(null);
    }

    public void addSessions(Session session) {
        sessions.add(session);
        session.setUser(this);
    }

    public void removeSessions(Session session) {
        sessions.remove(session);
        session.setUser(null);
    }

    public String getExternalID() {
        return cryptoDomainID.encrypt(ID);
    }

    public void setExternalID(String ID) {
        this.ID = getID(ID);
    }

    public static Long getID(String ID) {
        return cryptoDomainID.decryptLong(ID);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        return ID != null && ID.equals(((User) o).ID);
    }
    @Override
    public int hashCode() {
        return Objects.hash(getID());
    }
}
