package io.cerberus.wallet.db.models;

import io.cerberus.wallet.helpers.Enum;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import javax.persistence.FetchType;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
public class Status {
    @Transient public static Enum.CryptoDomain cryptoDomainID = Enum.CryptoDomain.StatusID;

    @Id private Integer ID;
    private String name;

    @OneToMany(mappedBy = "status", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER) private List<User> users = new ArrayList<>();
    @OneToMany(mappedBy = "status", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER) private List<Account> accounts = new ArrayList<>();

    public Status(Integer ID) {
        this.ID = ID;
    }

    public Status(String ID) {
        this(getID(ID));
    }

    public Status(Integer ID, String name) {
        this.ID = ID;
        this.name = name;
    }

    public Status(String ID, String name) {
        this(getID(ID), name);
    }

    public Integer getID() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public List<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<Account> accounts) {
        this.accounts = accounts;
    }

    /* ****** ?? ****** *//* ****** ?? ****** *//* ****** ?? ****** *//* ****** ?? ****** */
    /*                               RELATIONSHIP ADDITIONAL                              */
    /* ****** ?? ****** *//* ****** ?? ****** *//* ****** ?? ****** *//* ****** ?? ****** */

    public void addUser(User user) {
        users.add(user);
        user.setStatus(this);
    }

    public void removeUser(User user) {
        users.remove(user);
        user.setStatus(null);
    }

    public void addAccount(Account account) {
        accounts.add(account);
        account.setStatus(this);
    }

    public void removeAccount(Account account) {
        accounts.remove(account);
        account.setStatus(null);
    }

    public String getExternalID() {
        return cryptoDomainID.encrypt(ID);
    }

    public void setExternalID(String ID) {
        this.ID = getID(ID);
    }

    public static Integer getID(String ID) {
        return cryptoDomainID.decryptInteger(ID);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Status)) return false;
        return ID != null && ID.equals(((Status) o).ID);
    }
    @Override
    public int hashCode() {
        return Objects.hash(getID());
    }
}
