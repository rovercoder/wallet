package io.cerberus.wallet.db.models;

import io.cerberus.wallet.helpers.Enum;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.FetchType;
import javax.persistence.CascadeType;
import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
public class AccountType {
    @Transient public static Enum.CryptoDomain cryptoDomainID = Enum.CryptoDomain.AccountTypeID;

    @Id private Integer ID;
    private String name;

    @OneToMany(mappedBy = "accountType", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER) private List<Account> accounts = new ArrayList<>();

    public AccountType(Integer ID) {
        this.ID = ID;
    }

    public AccountType(String ID) {
        this(getID(ID));
    }

    public AccountType(Integer ID, String name) {
        this.ID = ID;
        this.name = name;
    }

    public AccountType(String ID, String name) {
        this(getID(ID), name);
    }

    public Integer getID() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<Account> accounts) {
        this.accounts = accounts;
    }

    /* ****** ?? ****** *//* ****** ?? ****** *//* ****** ?? ****** *//* ****** ?? ****** */
    /*                               RELATIONSHIP ADDITIONAL                              */
    /* ****** ?? ****** *//* ****** ?? ****** *//* ****** ?? ****** *//* ****** ?? ****** */

    public void addAccount(Account account) {
        accounts.add(account);
        account.setAccountType(this);
    }

    public void removeAccount(Account account) {
        accounts.remove(account);
        account.setAccountType(null);
    }

    public String getExternalID() {
        return cryptoDomainID.encrypt(ID);
    }

    public void setExternalID(String ID) {
        this.ID = getID(ID);
    }

    public static Integer getID(String ID) {
        return cryptoDomainID.decryptInteger(ID);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AccountType)) return false;
        return ID != null && ID.equals(((AccountType) o).ID);
    }
    @Override
    public int hashCode() {
        return Objects.hash(getID());
    }
}
