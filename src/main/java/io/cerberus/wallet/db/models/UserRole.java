package io.cerberus.wallet.db.models;

import io.cerberus.wallet.helpers.Enum;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.FetchType;
import javax.persistence.CascadeType;
import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
public class UserRole {
    @Transient public static Enum.CryptoDomain cryptoDomainID = Enum.CryptoDomain.UserRoleID;

    @Id private Integer ID;
    private String name;

    @OneToMany(mappedBy = "role", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER) private List<User> users = new ArrayList<>();

    public UserRole(Integer ID) {
        this.ID = ID;
    }

    public UserRole(String ID) {
        this(getID(ID));
    }

    public UserRole(Integer ID, String name) {
        this.ID = ID;
        this.name = name;
    }

    public UserRole(String ID, String name) {
        this(getID(ID), name);
    }

    public Integer getID() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    /* ****** ?? ****** *//* ****** ?? ****** *//* ****** ?? ****** *//* ****** ?? ****** */
    /*                               RELATIONSHIP ADDITIONAL                              */
    /* ****** ?? ****** *//* ****** ?? ****** *//* ****** ?? ****** *//* ****** ?? ****** */

    public void addUser(User user) {
        users.add(user);
        user.setRole(this);
    }

    public void removeUser(User user) {
        users.remove(user);
        user.setRole(null);
    }

    public String getExternalID() {
        return cryptoDomainID.encrypt(ID);
    }

    public void setExternalID(String ID) {
        this.ID = getID(ID);
    }

    public static Integer getID(String ID) {
        return cryptoDomainID.decryptInteger(ID);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserRole)) return false;
        return ID != null && ID.equals(((UserRole) o).ID);
    }
    @Override
    public int hashCode() {
        return Objects.hash(getID());
    }
}
