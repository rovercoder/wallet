package io.cerberus.wallet.db.models;

import io.cerberus.wallet.helpers.Enum;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.CascadeType;
import javax.persistence.Transient;
import java.util.List;
import java.util.Objects;

@Entity
public class AccountActivityGroup {
    @Transient public static Enum.CryptoDomain cryptoDomainID = Enum.CryptoDomain.AccountActivityGroupID;

    @Id @GeneratedValue private Long ID;
    @ManyToOne(fetch = FetchType.EAGER) @JoinColumn(name = "account_activity_group_type_id") private AccountActivityGroupType accountActivityGroupType;

    @OneToMany(mappedBy = "accountActivityGroup", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER) private List<AccountActivity> accountActivities;

    public AccountActivityGroup(Long ID) {
        this.ID = ID;
    }

    public AccountActivityGroup(String ID) {
        this(getID(ID));
    }

    public AccountActivityGroup(Long ID, Integer accountActivityGroupTypeID) {
        this.ID = ID;
        this.accountActivityGroupType = new AccountActivityGroupType(accountActivityGroupTypeID);
    }

    public AccountActivityGroup(String ID, String accountActivityGroupTypeID) {
        this(getID(ID), AccountActivityGroupType.getID(ID));
    }

    public Long getID() {
        return ID;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }

    public AccountActivityGroupType getAccountActivityGroupType() {
        return accountActivityGroupType;
    }

    public void setAccountActivityGroupType(AccountActivityGroupType accountActivityGroupType) {
        this.accountActivityGroupType = accountActivityGroupType;
    }

    public List<AccountActivity> getAccountActivities() {
        return accountActivities;
    }

    public void setAccountActivities(List<AccountActivity> accountActivities) {
        this.accountActivities = accountActivities;
    }

    /* ****** ?? ****** *//* ****** ?? ****** *//* ****** ?? ****** *//* ****** ?? ****** */
    /*                               RELATIONSHIP ADDITIONAL                              */
    /* ****** ?? ****** *//* ****** ?? ****** *//* ****** ?? ****** *//* ****** ?? ****** */

    public void addAccountActivity(AccountActivity accountActivity) {
        accountActivities.add(accountActivity);
        accountActivity.setAccountActivityGroup(this);
    }

    public void removeAccountActivity(AccountActivity accountActivity) {
        accountActivities.remove(accountActivity);
        accountActivity.setAccountActivityGroup(null);
    }

    public String getExternalID() {
        return cryptoDomainID.encrypt(ID);
    }

    public void setExternalID(String ID) {
        this.ID = getID(ID);
    }

    public static Long getID(String ID) {
        return cryptoDomainID.decryptLong(ID);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AccountActivityGroup)) return false;
        return ID != null && ID.equals(((AccountActivityGroup) o).ID);
    }
    @Override
    public int hashCode() {
        return Objects.hash(getID());
    }
}
