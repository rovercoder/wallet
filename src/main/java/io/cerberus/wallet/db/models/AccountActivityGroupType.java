package io.cerberus.wallet.db.models;

import io.cerberus.wallet.helpers.Enum;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.OneToMany;
import javax.persistence.FetchType;
import javax.persistence.CascadeType;
import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
public class AccountActivityGroupType {
    @Transient public static Enum.CryptoDomain cryptoDomainID = Enum.CryptoDomain.AccountActivityGroupTypeID;

    @Id @GeneratedValue private Integer ID;
    private String name;

    @OneToMany(mappedBy = "accountActivityGroupType", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER) private List<AccountActivityGroup> accountActivityGroups = new ArrayList<>();

    public AccountActivityGroupType(Integer ID) {
        this.ID = ID;
    }

    public AccountActivityGroupType(String ID) {
        this(getID(ID));
    }

    public AccountActivityGroupType(Integer ID, String name) {
        this.ID = ID;
        this.name = name;
    }

    public AccountActivityGroupType(String ID, String name) {
        this(getID(ID), name);
    }

    public Integer getID() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<AccountActivityGroup> getAccountActivityGroups() {
        return accountActivityGroups;
    }

    public void setAccountActivityGroups(List<AccountActivityGroup> accountActivityGroups) {
        this.accountActivityGroups = accountActivityGroups;
    }

    /* ****** ?? ****** *//* ****** ?? ****** *//* ****** ?? ****** *//* ****** ?? ****** */
    /*                               RELATIONSHIP ADDITIONAL                              */
    /* ****** ?? ****** *//* ****** ?? ****** *//* ****** ?? ****** *//* ****** ?? ****** */

    public void addAccountActivityGroup(AccountActivityGroup accountActivityGroup) {
        accountActivityGroups.add(accountActivityGroup);
        accountActivityGroup.setAccountActivityGroupType(this);
    }

    public void removeAccountActivityGroup(AccountActivityGroup accountActivityGroup) {
        accountActivityGroups.remove(accountActivityGroup);
        accountActivityGroup.setAccountActivityGroupType(null);
    }

    public String getExternalID() {
        return cryptoDomainID.encrypt(ID);
    }

    public void setExternalID(String ID) {
        this.ID = getID(ID);
    }

    public static Integer getID(String ID) {
        return cryptoDomainID.decryptInteger(ID);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AccountActivityGroupType)) return false;
        return ID != null && ID.equals(((AccountActivityGroupType) o).ID);
    }
    @Override
    public int hashCode() {
        return Objects.hash(getID());
    }
}
