package io.cerberus.wallet.db.models;

import io.cerberus.wallet.helpers.Enum;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.ManyToOne;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.Transient;
import java.time.DayOfWeek;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

@Entity
public class Session {
    @Transient public static Enum.CryptoDomain cryptoDomainID = Enum.CryptoDomain.SessionID;

    @Id @GeneratedValue private Long ID;
    private Date expiryDate = Date.from(Instant.now().plus(24, ChronoUnit.HOURS));

    @ManyToOne(fetch = FetchType.EAGER) @JoinColumn(name = "user_id") private User user;

    public Session() {
    }

    public Session(Long ID) {
        this.ID = ID;
    }

    public Session(String ID) {
        this(getID(ID));
    }

    public Session(Long ID, Date expiryDate) {
        this.ID = ID;
        this.expiryDate = expiryDate;
    }

    public Session(String ID, Date expiryDate) {
        this(getID(ID), expiryDate);
    }

    public Long getID() {
        return ID;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getExternalID() {
        return cryptoDomainID.encrypt(ID);
    }

    public void setExternalID(String ID) {
        this.ID = getID(ID);
    }

    public static Long getID(String ID) {
        return cryptoDomainID.decryptLong(ID);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Session)) return false;
        return ID != null && ID.equals(((Session) o).ID);
    }
    @Override
    public int hashCode() {
        return Objects.hash(getID());
    }
}
