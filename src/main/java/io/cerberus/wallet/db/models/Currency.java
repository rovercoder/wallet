package io.cerberus.wallet.db.models;

import io.cerberus.wallet.helpers.Enum;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.FetchType;
import javax.persistence.CascadeType;
import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
public class Currency {
    @Transient public static Enum.CryptoDomain cryptoDomainID = Enum.CryptoDomain.CurrencyID;

    @Id private Integer ID;
    private String code;
    private String name;
    private String symbol;
    private Integer decimals;

    @OneToMany(mappedBy = "currency", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER) private List<Account> accounts = new ArrayList<>();

    public Currency(Integer ID) {
        this.ID = ID;
    }

    public Currency(String ID) {
        this(getID(ID));
    }

    public Currency(Integer ID, String code, String name, String symbol, Integer decimals) {
        this.ID = ID;
        this.code = code;
        this.name = name;
        this.symbol = symbol;
        this.decimals = decimals;
    }

    public Currency(String ID, String code, String name, String symbol, Integer decimals) {
        this(getID(ID), code, name, symbol, decimals);
    }

    public Integer getID() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public Integer getDecimals() {
        return decimals;
    }

    public void setDecimals(Integer decimals) {
        this.decimals = decimals;
    }

    public List<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<Account> accounts) {
        this.accounts = accounts;
    }

    /* ****** ?? ****** *//* ****** ?? ****** *//* ****** ?? ****** *//* ****** ?? ****** */
    /*                               RELATIONSHIP ADDITIONAL                              */
    /* ****** ?? ****** *//* ****** ?? ****** *//* ****** ?? ****** *//* ****** ?? ****** */

    public void addAccount(Account account) {
        accounts.add(account);
        account.setCurrency(this);
    }

    public void removeAccount(Account account) {
        accounts.remove(account);
        account.setCurrency(null);
    }

    public String getExternalID() {
        return cryptoDomainID.encrypt(ID);
    }

    public void setExternalID(String ID) {
        this.ID = getID(ID);
    }

    public static Integer getID(String ID) {
        return cryptoDomainID.decryptInteger(ID);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Currency)) return false;
        return ID != null && ID.equals(((Currency) o).ID);
    }
    @Override
    public int hashCode() {
        return Objects.hash(getID());
    }
}
