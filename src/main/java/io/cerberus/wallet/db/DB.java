package io.cerberus.wallet.db;

import io.cerberus.wallet.db.repos.*;
import org.springframework.beans.factory.annotation.Autowired;

public class DB {
    @Autowired public AccountRepository accounts;
    @Autowired public AccountTypeRepository accountTypes;
    @Autowired public AccountActivityRepository accountActivities;
    @Autowired public AccountActivityGroupRepository accountActivityGroups;
    @Autowired public AccountActivityGroupTypeRepository accountActivityGroupTypes;
    @Autowired public AccountActivityTypeRepository accountActivityTypes;
    @Autowired public CurrencyRepository currencies;
    @Autowired public SessionRepository sessions;
    @Autowired public StatusRepository statuses;
    @Autowired public UserRepository users;
    @Autowired public UserRoleRepository userRoles;
}
