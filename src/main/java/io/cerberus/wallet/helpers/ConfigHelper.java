package io.cerberus.wallet.helpers;

import io.cerberus.wallet.WalletApplication;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ConfigHelper {

    public static Properties GetProperties() {

        Properties prop = new Properties();
        InputStream input = null;

        try {
            String filename = "config.properties";
            input = WalletApplication.class.getClassLoader().getResourceAsStream(filename);

            if (input == null) {
                System.out.println("Unable to find " + filename);
                return null;
            }

            prop.load(input);
            return prop;

        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (input != null){
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return null;
    }
}