package io.cerberus.wallet.helpers;

import io.cerberus.wallet.Context;
import io.cerberus.wallet.db.models.Session;
import org.apache.tomcat.util.codec.binary.Base64;

import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.SecureRandom;
import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

public class Crypto {

    private static int KEY_LENGTH = 256;
    //private static int ivSize = 128;
    private static int ITERATION_COUNT = 100;
    private static final int PKCS7_SALT_LENGTH = 16;
    private static final String PBKDF2_DERIVATION_ALGORITHM = "PBKDF2WithHmacSHA1";
    private static final String CIPHER_ALGORITHM = "AES/CBC/PKCS7Padding";
    private static final String DELIMITER = "]";
    private static final SecureRandom random = new SecureRandom();
    private static final String masterPassword = "g87&dnv&d@ZSrZ\\m";
    private static final String DATA_SUFFIX = "wPMmQjfclz";


    public static String encrypt(String message, Enum.CryptoDomain cryptoDomain) {
        try {
            byte[] salt = new byte[PKCS7_SALT_LENGTH];
            random.nextBytes(salt);
            SecretKey key = deriveKey(generateKeyBy(cryptoDomain, Context.getInstance().session, masterPassword), salt);

            Cipher cipher = Cipher.getInstance(CIPHER_ALGORITHM);
            byte[] iv = new byte[cipher.getBlockSize()];
            random.nextBytes(iv);

            cipher.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(iv));

            String saltString = Common.HexConvert.toHexFromBytes(salt);
            String ivString = Common.HexConvert.toHexFromBytes(iv);
            String encodedMessage = Base64.encodeBase64String(cipher.doFinal((message + DATA_SUFFIX).getBytes(StandardCharsets.UTF_8))); //, Base64.NO_WRAP);

            return saltString + ivString + encodedMessage;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String decrypt(String encryptedMessage, Enum.CryptoDomain cryptoDomain) {

        byte[] salt = Common.HexConvert.toBytesFromHex(encryptedMessage.substring(0, 32));
        byte[] iv = Common.HexConvert.toBytesFromHex(encryptedMessage.substring(32, 64));
        byte[] encrypted = Base64.decodeBase64(encryptedMessage.substring(64)); //, Base64.NO_WRAP);

        try {

            SecretKey key = deriveKey(generateKeyBy(cryptoDomain, Context.getInstance().session, masterPassword), salt);
            Cipher cipher = Cipher.getInstance(CIPHER_ALGORITHM);
            cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(iv));
            String decrypted = new String(cipher.doFinal(encrypted), StandardCharsets.UTF_8);
            if (!decrypted.endsWith(DATA_SUFFIX))
                throw new InvalidKeyException();

            return decrypted.substring(0, decrypted.lastIndexOf(DATA_SUFFIX));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private static String generateKeyBy(Enum.CryptoDomain cryptoDomain, Session session, String password) {
        String sessionID = session != null ? session.getID().toString() : "";
        return cryptoDomain.getID() + sessionID + password + sessionID + cryptoDomain.getID();
    }

    private static SecretKey deriveKey(String password, byte[] salt) {
        try {
            KeySpec keySpec = new PBEKeySpec(password.toCharArray(), salt, ITERATION_COUNT, KEY_LENGTH);
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(PBKDF2_DERIVATION_ALGORITHM);
            byte[] keyBytes = keyFactory.generateSecret(keySpec).getEncoded();
            return new SecretKeySpec(keyBytes, "AES");
        } catch (GeneralSecurityException e) {
            throw new RuntimeException(e);
        }
    }
}

