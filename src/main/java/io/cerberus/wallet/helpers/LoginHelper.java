package io.cerberus.wallet.helpers;

import io.cerberus.wallet.Context;
import io.cerberus.wallet.db.DB;
import io.cerberus.wallet.db.models.Session;
import io.cerberus.wallet.db.models.User;
import io.cerberus.wallet.models.request.LoginDTO;

public class LoginHelper {

//    public static void loginUser(LoginDTO loginDTO) {
//        loginDTO.validate();
//
//        DB db = Context.getInstance().db;
//
//        User user = db.users.findFirstByEmailEquals(loginDTO.email.trim());
//
//        boolean loggedIn = false;
//
//        //TODO: Remove for production!
//        if (user != null && user.getRole().getID().equals(Enum.UserRole.Admin.getID())) {
//            if (loginDTO.password.equals("strawberry"))
//                loggedIn = true;
//        }
//
//        if (loggedIn)
//            attachUserSession(db.sessions.save(new Session()));
//    }

//    private static void attachUserSession(Session session) {
//        Context context = Context.getInstance();
//        context.session = session;
//    }

    public static void loadUserSession(String sessionID) {
        Context context = Context.getInstance();
        context.session = context.db.sessions.validateAndFindById(sessionID, "sessionID");
    }
}
