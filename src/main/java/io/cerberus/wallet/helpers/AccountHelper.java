package io.cerberus.wallet.helpers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.cerberus.wallet.Context;
import io.cerberus.wallet.db.DB;
import io.cerberus.wallet.db.models.*;
import io.cerberus.wallet.db.models.Currency;
import io.cerberus.wallet.helpers.Common.string;
import io.cerberus.wallet.helpers.Common.field;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.security.InvalidParameterException;
import java.time.Duration;
import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

public class AccountHelper {

    public static Account transferExchangeBalance(String fromAccountID, String toAccountID, BigDecimal amount, boolean allowExchangeBetweenUserAccounts) {
        DB db = Context.getInstance().db;

        if (fromAccountID.trim().equals(toAccountID.trim()))
            field.throwInvalid("accountIDs");

        Account fromAccount = db.accounts.validateAndFindById(fromAccountID, "fromAccountID");
        Account toAccount = db.accounts.validateAndFindById(toAccountID, "toAccountID");

        boolean userIsOwnerOfBothAccounts = fromAccount.getUser().equals(toAccount.getUser());

        if ((allowExchangeBetweenUserAccounts && !userIsOwnerOfBothAccounts) || (!allowExchangeBetweenUserAccounts && userIsOwnerOfBothAccounts))
            field.throwInvalid("accountIDs");

        transferExchangeBalance(fromAccount, toAccount, amount);

        return db.accounts.findById(fromAccount.getCurrency().equals(toAccount.getCurrency()) ? Account.getID(fromAccountID) : Account.getID(toAccountID)).get();
    }

    public static void transferExchangeBalance(Account fromAccount, Account toAccount, BigDecimal amount) {
        if (fromAccount == null)
            throw new InvalidParameterException("Source User Account Non-Existant!");

        if (toAccount == null)
            throw new InvalidParameterException("Destination User Account Non-Existant!");

        if (!fromAccount.ensureAmountBalance(amount))
            throw new InvalidParameterException("Insufficient funds in source account");

        doTransaction(fromAccount.getCurrency().equals(toAccount.getCurrency()) ? Enum.AccountActivityGroupType.Transfer : Enum.AccountActivityGroupType.Exchange, fromAccount, toAccount, amount, false, false, false);
    }

    public static void transferExchangeBalance(Long fromUserID, Long toUserID, String fromCurrencyCode, String toCurrencyCode, BigDecimal amount) {
        DB db = Context.getInstance().db;

        final String _fromCurrencyCode = fromCurrencyCode.trim().toUpperCase();
        final String _toCurrencyCode = toCurrencyCode.trim().toUpperCase();

        boolean sameCurrency = _fromCurrencyCode.equals(_toCurrencyCode);

        if (fromUserID.equals(toUserID) && sameCurrency)
            throw new InvalidParameterException("Source and destination accounts cannot be the same");

        User fromUser = db.users.validateAndFindById(fromUserID, "fromUserID");
        User toUser = db.users.validateAndFindById(toUserID, "toUserID");

        List<Currency> currencies = db.currencies.findAllByCodeIn(Arrays.asList(_fromCurrencyCode, _toCurrencyCode));

        Currency fromAccountCurrency = currencies.stream().filter(x -> x.getCode().equals(_fromCurrencyCode)).findAny().orElse(null);
        Currency toAccountCurrency = currencies.stream().filter(x -> x.getCode().equals(_toCurrencyCode)).findAny().orElse(null);

        if (fromAccountCurrency == null)
            throw new InvalidParameterException("Source Currency Unsupported!");

        if (toAccountCurrency == null)
            throw new InvalidParameterException("Destination Currency Unsupported!");

        Account fromAccount = db.accounts.findFirstByUserAndCurrencyAndAccountTypeAndStatus(fromUser, fromAccountCurrency, Enum.AccountType.Payments.construct(), Enum.Status.Active.construct());
        Account toAccount = db.accounts.findFirstByUserAndCurrencyAndAccountTypeAndStatus(toUser, toAccountCurrency, Enum.AccountType.Payments.construct(), Enum.Status.Active.construct());

        transferExchangeBalance(fromAccount, toAccount, amount);
    }

    public static void transferExchangeBalance(String fromUserID, String toUserID, String fromCurrencyCode, String toCurrencyCode, BigDecimal amount) {
        transferExchangeBalance(User.getID(fromUserID), User.getID(toUserID), fromCurrencyCode, toCurrencyCode, amount);
    }

    public static Account depositBalance(Account forAccount, BigDecimal amount) {
        Account systemPaymentsInAccount = getActiveSystemAccount(forAccount.getCurrency(), Enum.AccountType.PaymentsIn);
        doTransaction(Enum.AccountActivityGroupType.Deposit, systemPaymentsInAccount, forAccount, amount, false, true, true);
        return forAccount;
    }

    public static Account depositBalance(Long forAccountID, BigDecimal amount) {
        DB db = Context.getInstance().db;
        return depositBalance(db.accounts.validateAndFindById(forAccountID, "forAccountID"), amount);
    }

    public static Account depositBalance(String forAccountID, BigDecimal amount) {
        return depositBalance(Account.getID(forAccountID), amount);
    }

    public static Account depositBalance(Long forUserID, String forCurrencyCode, BigDecimal amount) {
        DB db = Context.getInstance().db;

        User forUser = db.users.validateAndFindById(forUserID, "forUserID");
        Currency forAccountCurrency = db.currencies.findFirstByCode(forCurrencyCode.trim().toUpperCase());
        if (forAccountCurrency == null)
            field.throwInvalid("forCurrencyCode");
        Account forAccount = db.accounts.findFirstByUserAndCurrencyAndAccountTypeAndStatus(forUser, forAccountCurrency, Enum.AccountType.Payments.construct(), Enum.Status.Active.construct());
        if (forAccount == null)
            throw new InvalidParameterException("Account Non-Existant");

        return depositBalance(forAccount, amount);
    }

    public static Account depositBalance(String forUserID, String forCurrencyCode, BigDecimal amount) {
        return depositBalance(User.getID(forUserID), forCurrencyCode, amount);
    }

    public static Account withdrawBalance(Account forAccount, BigDecimal amount) {
        Account systemPaymentsOutAccount = getActiveSystemAccount(forAccount.getCurrency(), Enum.AccountType.PaymentsOut);
        doTransaction(Enum.AccountActivityGroupType.Withdrawal, forAccount, systemPaymentsOutAccount, amount, false, true, false);
        return forAccount;
    }

    public static Account withdrawBalance(Long forAccountID, BigDecimal amount) {
        DB db = Context.getInstance().db;
        return withdrawBalance(db.accounts.validateAndFindById(forAccountID, "forAccountID"), amount);
    }

    public static Account withdrawBalance(String forAccountID, BigDecimal amount) {
        return withdrawBalance(Account.getID(forAccountID), amount);
    }

    public static Account withdrawBalance(Long forUserID, String forCurrencyCode, BigDecimal amount) {
        DB db = Context.getInstance().db;

        User forUser = db.users.validateAndFindById(forUserID, "forUserID");
        Currency forAccountCurrency = db.currencies.findFirstByCode(forCurrencyCode.trim().toUpperCase());
        Account forAccount = db.accounts.findFirstByUserAndCurrencyAndAccountTypeAndStatus(forUser, forAccountCurrency, Enum.AccountType.Payments.construct(), Enum.Status.Active.construct());

        return withdrawBalance(forAccount, amount);
    }

    public static Account withdrawBalance(String forUserID, String forCurrencyCode, BigDecimal amount) {
        return withdrawBalance(User.getID(forUserID), forCurrencyCode, amount);
    }

    public static Account getActiveSystemAccount(Currency currency, Enum.AccountType accountType) {
        DB db = Context.getInstance().db;
        return db.accounts.findFirstByUserAndCurrencyAndAccountTypeAndStatus(new User(1L), currency, accountType.construct(), Enum.Status.Active.construct());
    }

    public static Account getActiveUserAccount(Long userID, Currency currency, Enum.AccountType accountType) {
        DB db = Context.getInstance().db;
        return db.accounts.findFirstByUserAndCurrencyAndAccountTypeAndStatus(new User(userID), currency, accountType.construct(), Enum.Status.Active.construct());
    }

    public static Account getActiveUserAccount(String userID, Currency currency, Enum.AccountType accountType) {
        return getActiveUserAccount(User.getID(userID), currency, accountType);
    }

    private static BigDecimal convertAmountToCurrency(String fromCurrencyCode, String toCurrencyCode, BigDecimal amount) {

        if (string.isNullOrWhiteSpace(fromCurrencyCode) || string.isNullOrWhiteSpace(toCurrencyCode) || fromCurrencyCode.equals(toCurrencyCode))
            return null;

        ConcurrentHashMap<String, Double> exchangeRates = fetchExchangeRates();
        Double fromAccountCurrencyRate = exchangeRates.get(fromCurrencyCode);
        Double toAccountCurrencyRate = exchangeRates.get(toCurrencyCode);

        return amount.multiply(new BigDecimal(toAccountCurrencyRate)).divide(new BigDecimal(fromAccountCurrencyRate));
    }

    public static BigDecimal getRate(Enum.AccountActivityGroupType accountActivityGroupType, Currency fromCurrency, Currency toCurrency, BigDecimal amount) {
        BigDecimal totalRate = new BigDecimal(0);

        switch (accountActivityGroupType) {
            case Deposit: //add Deposit charges
                break;
            case Exchange: //add Exchange charges
                if (fromCurrency.equals(toCurrency))
                    field.throwInvalid("accountIDs");
                totalRate = totalRate.add(getTransferExchangeRate(fromCurrency, toCurrency, amount));
                break;
            case Transfer: //add Transfer charges
                if (!fromCurrency.equals(toCurrency))
                    field.throwInvalid("accountIDs");
                totalRate = totalRate.add(getTransferExchangeRate(fromCurrency, toCurrency, amount));
                break;
            case Withdrawal: //add Withdrawal charges
                break;
        }

        return totalRate;
    }

    public static BigDecimal getTransferExchangeRate(boolean sameCurrency, BigDecimal amount) {
        Properties props = ConfigHelper.GetProperties();
        BigDecimal spreadPerc = new BigDecimal(Double.parseDouble(props.get(sameCurrency ? "transferSpreadPerc" : "exchangeSpreadPerc").toString()));
        BigDecimal spreadPTC = new BigDecimal(Double.parseDouble(props.get(sameCurrency ? "transferSpreadPTC" : "exchangeSpreadPTC").toString()));

        return amount.multiply((spreadPerc.divide(new BigDecimal(100), RoundingMode.CEILING))).add(spreadPTC);
    }

    public static BigDecimal getTransferExchangeRate(Currency fromCurrency, Currency toCurrency, BigDecimal amount) {
        return getTransferExchangeRate(fromCurrency.equals(toCurrency), amount);
    }

    private static ConcurrentHashMap<String, Double> fetchExchangeRates() {

        Context context = Context.getInstance();
        Properties props = ConfigHelper.GetProperties();

        List<String> currencyCodes = new ArrayList<>();
        context.db.currencies.findAll().forEach(x -> currencyCodes.add(x.getCode()));

        String fixerIO_APIKey = props.get("fixerIO_APIKey").toString();
        String baseCurrencyCode = props.get("baseCurrencyCode").toString();
        String currencyCodesDelimited = String.join(",", currencyCodes);

        String cacheKey = "exchangeRates_" + baseCurrencyCode + "_" + currencyCodesDelimited;
        ConcurrentHashMap<String, Double> exchangeRatesFound = (ConcurrentHashMap<String, Double>)context.memCache.get(cacheKey);
        if (exchangeRatesFound != null)
            return exchangeRatesFound;

        try {
            String exchangeRates = Request.Get.Send(
                String.format("http://data.fixer.io/api/latest?access_key=%s&base=%s&symbols=%s",
                    fixerIO_APIKey,
                    baseCurrencyCode,
                    currencyCodesDelimited
                )
            );
            ObjectMapper mapper = new ObjectMapper();
            JsonNode rates = mapper.readTree(exchangeRates).get("rates");
            exchangeRatesFound = (ConcurrentHashMap<String, Double>)mapper.convertValue(rates, ConcurrentHashMap.class);
            context.memCache.add(cacheKey, exchangeRatesFound, Duration.ofHours(1).toMillis());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return exchangeRatesFound;
    }

    private static BigDecimal getAmount(Enum.AccountActivityGroupType accountActivityGroupType, BigDecimal amount, boolean incFee, Currency fromCurrency, Currency toCurrency, boolean withFee) {
        if (!incFee && !withFee || incFee && withFee)
            return amount;

        BigDecimal fee = getAmountFee(accountActivityGroupType, amount, incFee, fromCurrency, toCurrency);

        return incFee ? amount.subtract(fee) : amount.add(fee);
    }

    private static BigDecimal getAmountFee(Enum.AccountActivityGroupType accountActivityGroupType, BigDecimal amount, boolean incFee, Currency fromCurrency, Currency toCurrency) {
        BigDecimal fee = getRate(accountActivityGroupType, fromCurrency, toCurrency, amount);
        return !incFee ? fee : fee.divide(amount, RoundingMode.CEILING);
    }

    private static void doTransaction(Enum.AccountActivityGroupType accountActivityGroupType, Account fromAccount, Account toAccount, BigDecimal fromAmount, boolean incSpread, boolean bypassSpread, boolean bypassBalanceCheck) {

        DB db = Context.getInstance().db;

        if (fromAccount == null || toAccount == null)
            throw new InvalidParameterException("Specified Account(s) Non-Existant!");
        else if (fromAccount.getID().equals(toAccount.getID())) {
            throw new InvalidParameterException("Source and destination accounts cannot be the same");
        }

        if (fromAmount.signum() <= 0)
            field.throwInvalid("fromAmount");

        Currency fromCurrency = fromAccount.getCurrency();
        Currency toCurrency = toAccount.getCurrency();

        boolean sameCurrency = fromCurrency.getID().equals(toCurrency.getID());

        BigDecimal fromAmountWithFee = !bypassSpread ? getAmount(accountActivityGroupType, fromAmount, incSpread, fromCurrency, toCurrency, true) : fromAmount;
        BigDecimal fromAmountWithoutFee = !bypassSpread ? getAmount(accountActivityGroupType, fromAmount, incSpread, fromCurrency, toCurrency, false) : fromAmount;

        if (fromAmountWithoutFee.signum() <= 0)
            throw new InvalidParameterException("Not enough balance!");

        BigDecimal walletFee = fromAmountWithFee.subtract(fromAmountWithoutFee);

        BigDecimal amountTo = sameCurrency ? fromAmountWithoutFee : convertAmountToCurrency(fromCurrency.getCode(), toCurrency.getCode(), fromAmountWithoutFee);

        if (!bypassBalanceCheck && !fromAccount.ensureAmountBalance(fromAmountWithFee))
            throw new InvalidParameterException("Not enough balance!");

        Account systemOperationsAccount = getActiveSystemAccount(fromCurrency, Enum.AccountType.Operations);
        Account systemCurrencyAccount = !sameCurrency ? getActiveSystemAccount(fromCurrency, Enum.AccountType.Currency) : null;
        Account systemFeesAccount = getActiveSystemAccount(fromCurrency, Enum.AccountType.Fees);
        Account systemCurrencyAccountToCurrency = !sameCurrency ? getActiveSystemAccount(toCurrency, Enum.AccountType.Currency) : null;
        Account systemExchangeAccountToCurrency = !sameCurrency ? getActiveSystemAccount(toCurrency, Enum.AccountType.Exchange) : null;
        Account systemFeesAccountToCurrency = !sameCurrency ? getActiveSystemAccount(toCurrency, Enum.AccountType.Fees) : null;

        AccountActivityGroup accountActivityGroup = new AccountActivityGroup(null, accountActivityGroupType.getID());

        List<AccountActivity> accountActivities = new ArrayList<>();

        fromAccount.remAmountBalance(fromAmountWithFee);
        systemOperationsAccount.addAmountBalance(fromAmountWithoutFee);
        accountActivities.add(new AccountActivity(null, fromAmountWithFee, null, Enum.AccountActivityType.Transfer.getID(), fromAccount.getID(), systemOperationsAccount.getID()));
        if (walletFee.signum() > 0) {
            systemFeesAccount.addAmountBalance(walletFee);
            accountActivities.add(new AccountActivity(null, walletFee, null, Enum.AccountActivityType.Transfer.getID(), fromAccount.getID(), systemFeesAccount.getID()));
        }

        if (!sameCurrency) {
            systemOperationsAccount.remAmountBalance(fromAmountWithoutFee).validateAmountBalance(bypassBalanceCheck);
            systemCurrencyAccount.addAmountBalance(fromAmountWithoutFee);
            accountActivities.add(new AccountActivity(null, fromAmountWithoutFee, null, Enum.AccountActivityType.Transfer.getID(), systemOperationsAccount.getID(), systemCurrencyAccount.getID()));

            systemCurrencyAccountToCurrency.remAmountBalance(amountTo).validateAmountBalance(bypassBalanceCheck);
            systemExchangeAccountToCurrency.addAmountBalance(amountTo);
            accountActivities.add(new AccountActivity(null, amountTo, null, Enum.AccountActivityType.Transfer.getID(), systemCurrencyAccountToCurrency.getID(), systemExchangeAccountToCurrency.getID()));

            systemExchangeAccountToCurrency.remAmountBalance(amountTo).validateAmountBalance(bypassBalanceCheck);
            toAccount.addAmountBalance(amountTo);
            accountActivities.add(new AccountActivity(null, amountTo, null, Enum.AccountActivityType.Transfer.getID(), systemExchangeAccountToCurrency.getID(), toAccount.getID()));
        } else {
            systemOperationsAccount.remAmountBalance(amountTo).validateAmountBalance(bypassBalanceCheck);
            toAccount.addAmountBalance(amountTo);
            accountActivities.add(new AccountActivity(null, amountTo, null, Enum.AccountActivityType.Transfer.getID(), systemOperationsAccount.getID(), toAccount.getID()));
        }

        final AccountActivityGroup accountActivityGroupFinal = db.accountActivityGroups.save(accountActivityGroup);
        accountActivities.forEach(x -> x.setAccountActivityGroup(accountActivityGroupFinal));
        db.accountActivities.saveAll(accountActivities);
        db.accounts.saveAll(!sameCurrency
                ? Arrays.asList(fromAccount, toAccount, systemOperationsAccount, systemFeesAccount, systemCurrencyAccount, systemCurrencyAccountToCurrency, systemExchangeAccountToCurrency, systemFeesAccountToCurrency)
                : Arrays.asList(fromAccount, toAccount, systemOperationsAccount, systemFeesAccount));
    }
}
