package io.cerberus.wallet.helpers;

import java.lang.reflect.Type;

import io.cerberus.wallet.Context;
import io.cerberus.wallet.db.models.Status;
import io.cerberus.wallet.helpers.Common.string;

public class Enum {

    public enum Status {
        Active(1),
        Suspended(2),
        Retired(3);

        private int _value;

        Status(int value) {
            this._value = value;
        }

        public int getID() {
            return _value;
        }

        public Status unless(Integer value) {
            if (value != null)
                this._value = value;
            return this;
        }

        public io.cerberus.wallet.db.models.Status construct() {
            return new io.cerberus.wallet.db.models.Status(_value);
        }

        public io.cerberus.wallet.db.models.Status request() {
            return Context.getInstance().db.statuses.validateAndFindById(_value, "Status");
        }
    }

    public enum UserRole {
        Admin(1),
        Normie(2);

        private int _value;

        UserRole(int value) {
            this._value = value;
        }

        public int getID() {
            return _value;
        }

        public UserRole unless(Integer value) {
            if (value != null)
                this._value = value;
            return this;
        }

        public boolean check() {
            return Context.getInstance().user.getRole().getID().equals(_value);
        }

        public io.cerberus.wallet.db.models.UserRole construct() {
            return new io.cerberus.wallet.db.models.UserRole(_value);
        }

        public io.cerberus.wallet.db.models.UserRole request() {
            return Context.getInstance().db.userRoles.validateAndFindById(_value, "UserRole");
        }
    }

    public enum AccountType {
        Currency(1),
        Payments(2),
        Exchange(3),
        Operations(4),
        Fees(5),
        PaymentsIn(6),
        PaymentsOut(7);

        private int _value;

        AccountType(int value) {
            this._value = value;
        }

        public int getID() {
            return _value;
        }

        public AccountType unless(Integer value) {
            if (value != null)
                this._value = value;
            return this;
        }

        public io.cerberus.wallet.db.models.AccountType construct() {
            return new io.cerberus.wallet.db.models.AccountType(_value);
        }

        public io.cerberus.wallet.db.models.AccountType request() {
            return Context.getInstance().db.accountTypes.validateAndFindById(_value, "AccountType");
        }
    }

    public enum AccountActivityType {
        Load(1),
        Exchange(2),
        Transfer(3),
        Unload(4);

        private int _value;

        AccountActivityType(int value) {
            this._value = value;
        }

        public int getID() {
            return _value;
        }

        public AccountActivityType unless(Integer value) {
            if (value != null)
                this._value = value;
            return this;
        }

        public io.cerberus.wallet.db.models.AccountActivityType construct() {
            return new io.cerberus.wallet.db.models.AccountActivityType(_value);
        }

        public io.cerberus.wallet.db.models.AccountActivityType request() {
            return Context.getInstance().db.accountActivityTypes.validateAndFindById(_value, "AccountActivityType");
        }
    }

    public enum AccountActivityGroupType {
        Deposit(1),
        Exchange(2),
        Transfer(3),
        Withdrawal(4);

        private int _value;

        AccountActivityGroupType(int value) {
            this._value = value;
        }

        public int getID() {
            return _value;
        }

        public AccountActivityGroupType unless(Integer value) {
            if (value != null)
                this._value = value;
            return this;
        }

        public io.cerberus.wallet.db.models.AccountActivityGroupType construct() {
            return new io.cerberus.wallet.db.models.AccountActivityGroupType(_value);
        }

        public io.cerberus.wallet.db.models.AccountActivityGroupType request() {
            return Context.getInstance().db.accountActivityGroupTypes.validateAndFindById(_value, "AccountActivityGroupType");
        }
    }

    public enum CryptoDomain {
        SessionID(1),
        UserID(2),
        AccountID(3),
        CurrencyID(4),
        StatusID(5),
        AccountTypeID(6),
        AccountActivityID(7),
        AccountActivityTypeID(8),
        AccountActivityGroupID(9),
        AccountActivityGroupTypeID(10),
        UserRoleID(11);

        private int _value;

        CryptoDomain(int value) {
            this._value = value;
        }

        public int getID() {
            return _value;
        }

        public String decrypt(String id) {
            if (string.isNullOrWhiteSpace(id))
                return null;
            id = Crypto.decrypt(id, this);
            if (string.isNullOrWhiteSpace(id))
                return null;
            return id;
        }

        public Long decryptLong(String id) {
            try {
                return Long.parseUnsignedLong(decrypt(id));
            } catch (Exception e) {}
            return null;
        }

        public Integer decryptInteger(String id) {
            try {
                return Integer.parseUnsignedInt(decrypt(id));
            } catch (Exception e) {}
            return null;
        }

        public String encrypt(Object id) {
            if (id == null)
                return null;
            return Crypto.encrypt(id.toString(), this);
        }
    }
}
