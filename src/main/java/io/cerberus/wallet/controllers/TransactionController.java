package io.cerberus.wallet.controllers;

import io.cerberus.wallet.db.models.Account;
import io.cerberus.wallet.helpers.AccountHelper;
import io.cerberus.wallet.helpers.Common.string;
import io.cerberus.wallet.helpers.Common.field;
import io.cerberus.wallet.helpers.Enum;
import io.cerberus.wallet.models.request.AccountBalanceDTO;
import io.cerberus.wallet.models.request.TransferDTO;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpServerErrorException;

import java.math.BigDecimal;

@RestController
@RequestMapping("/api/{sessionID}")
public class TransactionController extends BaseController {

    @GetMapping("/rate/{transactionType}/{fromAccountID}/{toAccountID}")
    public BigDecimal getRate(@PathVariable("sessionID") String sessionID, @PathVariable("transactionType") String transactionType, @PathVariable("fromAccountID") String fromAccountID, @PathVariable("toAccountID") String toAccountID, @RequestParam("amount") BigDecimal amount) {
        checkSession(sessionID);
        Enum.AccountActivityGroupType accountActivityGroupType = null;
        boolean singleAccount = false;
        switch (transactionType.trim().toLowerCase()) {
            case "topup":
                accountActivityGroupType = Enum.AccountActivityGroupType.Deposit;
                singleAccount = true;
                if (!string.isNullOrWhiteSpace(toAccountID))
                    throw new HttpServerErrorException(HttpStatus.NOT_FOUND);
                toAccountID = fromAccountID;
                fromAccountID = null;
            break;
            case "exchange":
                accountActivityGroupType = Enum.AccountActivityGroupType.Exchange;
            break;
            case "transfer":
                accountActivityGroupType = Enum.AccountActivityGroupType.Transfer;
            break;
            case "withdraw":
                accountActivityGroupType = Enum.AccountActivityGroupType.Withdrawal;
                singleAccount = true;
                if (!string.isNullOrWhiteSpace(toAccountID))
                    throw new HttpServerErrorException(HttpStatus.NOT_FOUND);
            break;
            default:
                throw new HttpServerErrorException(HttpStatus.NOT_FOUND);
        }
        return getRate(fromAccountID, toAccountID, amount, accountActivityGroupType, singleAccount);
    }

    @PutMapping("/topup/{accountID}/{amount}")
    public AccountBalanceDTO performTopUp(@PathVariable("sessionID") String sessionID, @PathVariable("accountID") String accountID, @PathVariable("amount") BigDecimal amount) {
        checkSession(sessionID, Enum.UserRole.Admin);
        Account account = AccountHelper.depositBalance(accountID, amount);
        return new AccountBalanceDTO(account);
    }

//    @PutMapping("/topupByCurrency/{currencyCode}/{amount}")
//    public AccountBalanceDTO topUpBalance(@PathVariable("sessionID") String sessionID, @PathVariable("currencyCode") String currencyCode, @PathVariable("amount") BigDecimal amount, @RequestParam(value = "userID") String userID) {
//        checkSession(sessionID, Enum.UserRole.Admin);
//        Account account = AccountHelper.depositBalance(userID, currencyCode, amount);
//        return new AccountBalanceDTO(account);
//    }

    @PutMapping("/exchange/{fromAccountID}/{toAccountID}")
    public AccountBalanceDTO performExchange(@PathVariable("sessionID") String sessionID, @PathVariable("fromAccountID") String fromAccountID, @PathVariable("toAccountID") String toAccountID, @RequestParam("amount") BigDecimal amount) {
        checkSession(sessionID);
        Account account = AccountHelper.transferExchangeBalance(fromAccountID, toAccountID, amount, true);
        return new AccountBalanceDTO(account);
    }

    @PutMapping("/transfer/{fromAccountID}/{toAccountID}")
    public AccountBalanceDTO performTransfer(@PathVariable("sessionID") String sessionID, @PathVariable("fromAccountID") String fromAccountID, @PathVariable("toAccountID") String toAccountID, @RequestParam("amount") BigDecimal amount) {
        checkSession(sessionID);
        Account account = AccountHelper.transferExchangeBalance(fromAccountID, toAccountID, amount, false);
        return new AccountBalanceDTO(account);
    }

    @PutMapping("/withdraw/{accountID}/{amount}")
    public AccountBalanceDTO performWithdrawal(@PathVariable("sessionID") String sessionID, @PathVariable("accountID") String accountID, @PathVariable("amount") BigDecimal amount) {
        checkSession(sessionID, Enum.UserRole.Admin);
        Account account = AccountHelper.withdrawBalance(accountID, amount);
        return new AccountBalanceDTO(account);
    }

//    @PutMapping("/balance/{currencyCode}/withdraw/{amount}")
//    public AccountBalanceDTO withdrawBalance(@PathVariable("sessionID") String sessionID, @PathVariable("accountID") String accountID, @PathVariable("amount") BigDecimal amount) {
//        checkSession(sessionID);
//        Account account = AccountHelper.withdrawBalance(accountID, amount);
//        return new AccountBalanceDTO(account);
//    }



    private BigDecimal getRate(String fromAccountID, String toAccountID, BigDecimal amount, Enum.AccountActivityGroupType accountActivityGroupType, boolean singleAccount) {

        if (amount.signum() <= 0)
            field.throwInvalid("amount");

        boolean fromAccountIDSpecified = !string.isNullOrWhiteSpace(fromAccountID);
        boolean toAccountIDSpecified = !string.isNullOrWhiteSpace(toAccountID);

        if (singleAccount) {
            if (fromAccountIDSpecified && toAccountIDSpecified)
                field.throwInvalid("accountID");
        } else if (!(fromAccountIDSpecified && toAccountIDSpecified))
            field.throwInvalid("accountID");

        if (fromAccountID.trim().equals(toAccountID.trim()))
            field.throwInvalid("accountIDs");

        Account fromAccount = fromAccountIDSpecified ? db.accounts.validateAndFindById(fromAccountID, "fromAmountID") : null;
        Account toAccount = toAccountIDSpecified ? db.accounts.validateAndFindById(toAccountID, "toAccountID") : null;

        boolean isAdmin = Enum.UserRole.Admin.check();

        if (!isAdmin && ((fromAccount != null && !fromAccount.getUser().equals(context.user)) || (toAccount != null && !toAccount.getUser().equals(context.user))))
            field.throwInvalid("accountIDs");

        return AccountHelper.getRate(accountActivityGroupType, fromAccount != null ? fromAccount.getCurrency() : null, toAccount != null ? toAccount.getCurrency() : null, amount);
    }


}