package io.cerberus.wallet.controllers;

import io.cerberus.wallet.Context;
import io.cerberus.wallet.db.DB;
import io.cerberus.wallet.helpers.Enum;
import io.cerberus.wallet.helpers.LoginHelper;
import org.hibernate.SessionException;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.HttpStatusCodeException;

public class BaseController {

    public Context context = Context.getInstance();
    public DB db = Context.getInstance().db;

    //TODO: get rid of sessionID everywhere -> http://deventh.blogspot.com/2017/07/spring-mvc-requestbodyadvice-or-how-to.html
    //TODO: check user role e.g. admin
    public void checkSession(String sessionID, Enum.UserRole userRole) {
        LoginHelper.loadUserSession(sessionID);
        if (context.session == null)
            throw new SessionException("Session non-existant or expired");
        else if (userRole != null && !userRole.check())
            throw new HttpServerErrorException(HttpStatus.METHOD_NOT_ALLOWED);
    }

    public void checkSession(String sessionID) {
        checkSession(sessionID, null);
    }
}
