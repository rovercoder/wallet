package io.cerberus.wallet.controllers;

import io.cerberus.wallet.helpers.LoginHelper;
import io.cerberus.wallet.models.request.InfoDTO;
import io.cerberus.wallet.models.request.LoginDTO;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/session/{sessionID}")
public class SessionController extends BaseController {

//    @PutMapping("/login")
//    public InfoDTO login(LoginDTO loginDTO) {
//        LoginHelper.loginUser(loginDTO);
//        return new InfoDTO();
//    }

    @GetMapping("/")
    public InfoDTO info(@PathVariable("sessionID") String sessionID) {
        checkSession(sessionID);
        return new InfoDTO();
    }
}