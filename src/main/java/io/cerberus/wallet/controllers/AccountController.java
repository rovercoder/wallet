package io.cerberus.wallet.controllers;

import io.cerberus.wallet.db.models.*;
import io.cerberus.wallet.helpers.AccountHelper;
import io.cerberus.wallet.helpers.Common.string;
import io.cerberus.wallet.helpers.Common.field;
import io.cerberus.wallet.models.request.AccountBalanceDTO;
import io.cerberus.wallet.models.request.AccountDTO;
import io.cerberus.wallet.models.request.AccountActivityDTO;
import io.cerberus.wallet.helpers.Enum;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpServerErrorException;

import java.math.BigDecimal;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/{sessionID}")
public class AccountController extends BaseController {

    @GetMapping("/account/{accountID}")
    public AccountDTO getAccount(@PathVariable("sessionID") String sessionID, @PathVariable("accountID") String accountID) {
        checkSession(sessionID);

        boolean isAdmin = Enum.UserRole.Admin.check();
        Account account = db.accounts.validateAndFindById(accountID, "accountID");

        if (!isAdmin && !account.getUser().equals(context.user))
            throw new HttpServerErrorException(HttpStatus.UNAUTHORIZED);

        return new AccountDTO(account);
    }

    @GetMapping("/accountByCurrency/{currencyCode}/{accountTypeID}")
    public AccountDTO getAccountByCurrency(@PathVariable("sessionID") String sessionID, @PathVariable("currencyCode") String currencyCode, @PathVariable(value = "accountTypeID", required = false) String accountTypeID) {
        checkSession(sessionID);

        boolean isAdmin = Enum.UserRole.Admin.check();
        Currency currency = verifyCurrency(currencyCode, "currencyCode", true);

        AccountType accountType = !string.isNullOrWhiteSpace(accountTypeID) ? db.accountTypes.validateAndFindById(accountTypeID, "accountTypeID") : Enum.AccountType.Payments.request();

        if (!isAdmin && accountType != null && !accountType.equals(Enum.AccountType.Payments.construct()))
            throw new HttpServerErrorException(HttpStatus.BAD_REQUEST);

        Account account = db.accounts.findFirstByUserAndCurrencyAndAccountTypeAndStatus(context.user, currency, accountType, Enum.Status.Active.construct());
        if (account == null)
            throw new InvalidParameterException("Account Non-Existant");

        return new AccountDTO(account);
    }

    @GetMapping("/user/{userID}/accountByCurrency/{currencyCode}/{accountTypeID}")
    public AccountDTO getUserAccountByCurrency(@PathVariable("sessionID") String sessionID, @PathVariable("userID") String userID, @PathVariable("currencyCode") String currencyCode, @PathVariable(value = "accountTypeID", required = false) String accountTypeID) {
        checkSession(sessionID, Enum.UserRole.Admin);

        User user = db.users.validateAndFindById(userID, "userID");
        Currency currency = verifyCurrency(currencyCode, "currencyCode", true);
        AccountType accountType = !string.isNullOrWhiteSpace(accountTypeID) ? db.accountTypes.validateAndFindById(accountTypeID, "accountTypeID") : Enum.AccountType.Payments.request();

        Account account = db.accounts.findFirstByUserAndCurrencyAndAccountTypeAndStatus(user, currency, accountType, Enum.Status.Active.construct());
        if (account == null)
            throw new InvalidParameterException("Account Non-Existant");

        return new AccountDTO(account);
    }

    @GetMapping("/user/{userID}/accounts")
    public List<AccountDTO> getUserAccounts(@PathVariable("sessionID") String sessionID, @PathVariable("userID") String userID) {
        checkSession(sessionID, Enum.UserRole.Admin);
        User user = db.users.validateAndFindById(userID, "userID");
        List<Account> accounts = db.accounts.findAllByUser(user);
        return accounts.stream().map(AccountDTO::new).collect(Collectors.toList());
    }

    @PutMapping("/account/{accountID}")
    public AccountDTO createOrUpdateAccount(@PathVariable("sessionID") String sessionID, @PathVariable("accountID") String accountID, AccountDTO account) {
        checkSession(sessionID, Enum.UserRole.Admin);

        if (string.isNullOrWhiteSpace(accountID))
            accountID = account.ID;

        boolean accountIDSpecified = !string.isNullOrWhiteSpace(accountID);

        Account _account = account.get();

        if (accountIDSpecified) {
            Account checkedAccount = db.accounts.validateAndFindById(accountID, "accountID");
            _account.setID(checkedAccount.getID());
            _account.setCurrency(checkedAccount.getCurrency());
            _account.setAmountBalance(checkedAccount.getAmountBalance());
            _account.setStatus(checkedAccount.getStatus());
            _account.setUser(checkedAccount.getUser());
        } else {
            _account.setAmountBalance(new BigDecimal(0));
            _account.setStatus(Enum.Status.Active.construct());
        }

        return new AccountDTO(db.accounts.save(_account));
    }

    @GetMapping("/account/{accountID}/verify/{amount}")
    public boolean verifyAccountBalance(@PathVariable("sessionID") String sessionID, @PathVariable("accountID") String accountID, @PathVariable("amount") BigDecimal amount) {
        checkSession(sessionID);
        Account account = db.accounts.validateAndFindById(accountID, "accountID");
        return account.ensureAmountBalance(amount);
    }

    @GetMapping("/account/{accountID}/transactions")
    public List<AccountActivityDTO> getAccountHistory(@PathVariable("sessionID") String sessionID, @PathVariable("accountID") String accountID) {
        checkSession(sessionID);

        boolean isAdmin = Enum.UserRole.Admin.check();

        Account account = db.accounts.validateAndFindById(accountID, "accountID");
        if (!isAdmin && !account.getUser().equals(context.user))
            throw new HttpServerErrorException(HttpStatus.BAD_REQUEST);

        List<AccountActivity> accountActivities = new ArrayList<>();
        accountActivities.addAll(account.getFromAccountActivities());
        accountActivities.addAll(account.getToAccountActivities());
        accountActivities.sort(Comparator.comparing(AccountActivity::getID));

        return accountActivities.stream().map(AccountActivityDTO::new).collect(Collectors.toList());
    }

//    private Account getActiveUserAccountByCurrencyCode(String userID, String currencyCode) {
//        Currency currency = context.db.currencies.findFirstByCode(currencyCode.trim().toUpperCase());
//        if (currency == null)
//            throw new InvalidParameterException("Specified currency invalid");
//
//        return db.accounts.findFirstByUserAndCurrencyAndAccountTypeAndStatus(new User(userID.trim()), currency, Enum.AccountType.Payments.construct(), Enum.Status.Active.construct());
//    }

    private Currency verifyAmountAndCurrency(BigDecimal amount, String currencyCode, String currencyCodeFieldDefinition, boolean currencyRequired) {
        if (amount.signum() <= 0)
            field.throwInvalid("amount");

        return verifyCurrency(currencyCode, currencyCodeFieldDefinition, currencyRequired);
    }

    private Currency verifyCurrency(String currencyCode, String currencyCodeFieldDefinition, boolean required) {
        Currency currency = null;
        if (!string.isNullOrWhiteSpace(currencyCode)) {
            currency = db.currencies.findFirstByCode(currencyCode.trim().toUpperCase());
            if (currency == null)
                field.throwInvalid(currencyCodeFieldDefinition);
        } else if (required) {
            field.throwInvalid(currencyCodeFieldDefinition);
        }
        return currency;
    }
}