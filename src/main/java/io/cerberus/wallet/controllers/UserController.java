package io.cerberus.wallet.controllers;

import io.cerberus.wallet.db.models.Account;
import io.cerberus.wallet.db.models.AccountType;
import io.cerberus.wallet.db.models.Currency;
import io.cerberus.wallet.db.models.User;
import io.cerberus.wallet.helpers.Common.field;
import io.cerberus.wallet.helpers.Common.string;
import io.cerberus.wallet.helpers.Enum;
import io.cerberus.wallet.models.request.AccountDTO;
import io.cerberus.wallet.models.request.UserDTO;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.client.HttpServerErrorException;

import java.security.InvalidParameterException;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RestController
@RequestMapping("/api/{sessionID}")
public class UserController extends BaseController {

    @GetMapping("/user/{userID}")
    public UserDTO getUser(@PathVariable("sessionID") String sessionID, @PathVariable("userID") String userID) {
        checkSession(sessionID);

        boolean isAdmin = Enum.UserRole.Admin.check();
        boolean userIDSpecified = !string.isNullOrWhiteSpace(userID);

        if (userIDSpecified && !isAdmin)
            throw new HttpServerErrorException(HttpStatus.NOT_FOUND);

        User user = userIDSpecified ? db.users.validateAndFindById(userID, "userID") : context.user;
        return new UserDTO(user);
    }

    @PutMapping("/user/{userID}")
    public UserDTO createOrUpdateUser(@PathVariable("sessionID") String sessionID, @PathVariable("userID") String userID, UserDTO user) {
        checkSession(sessionID);

        boolean isAdmin = Enum.UserRole.Admin.check();

        if (string.isNullOrWhiteSpace(userID))
            userID = user.ID;

        boolean userIDSpecified = !string.isNullOrWhiteSpace(userID);

        User _user = user.get();

        if (userIDSpecified && !isAdmin)
            throw new HttpServerErrorException(HttpStatus.NOT_FOUND);

        if (userIDSpecified) {
            User checkedUser = db.users.validateAndFindById(userID, "userID");
            _user.setID(checkedUser.getID());
        }
        else {
            _user.setID(context.user.getID());
            _user.setStatus(context.user.getStatus());
        }

        return new UserDTO(db.users.save(_user));
    }

    @GetMapping("/users")
    public List<UserDTO> getUsers(@PathVariable("sessionID") String sessionID) {
        checkSession(sessionID, Enum.UserRole.Admin);
        return StreamSupport.stream(db.users.findAll().spliterator(), false).map(UserDTO::new).collect(Collectors.toList());
    }
}