package io.cerberus.wallet;

import io.cerberus.wallet.db.DB;
import io.cerberus.wallet.db.models.Session;
import io.cerberus.wallet.db.models.User;
import io.cerberus.wallet.models.InMemoryCacheWithDelayQueue;

public class Context {
    public DB db = new DB();
    public Session session = null;
    public User user = null;
    public InMemoryCacheWithDelayQueue memCache = new InMemoryCacheWithDelayQueue();

    // static variable single_instance of type Singleton
    private static Context instance = null;

    // private constructor restricted to this class itself
    private Context() {}

    // static method to create instance of Singleton class
    public static Context getInstance()
    {
        if (instance == null)
            instance = new Context();

        return instance;
    }
}
