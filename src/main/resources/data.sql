INSERT INTO Account_Type VALUES(1, 'Currency');
INSERT INTO Account_Type VALUES(2, 'Payments');
INSERT INTO Account_Type VALUES(3, 'Exchange');
INSERT INTO Account_Type VALUES(4, 'Operations');
INSERT INTO Account_Type VALUES(5, 'Fees');
INSERT INTO Account_Type VALUES(6, 'PaymentsIn');
INSERT INTO Account_Type VALUES(7, 'PaymentsOut');

INSERT INTO Account_Activity_Type VALUES(1, 'Load');
INSERT INTO Account_Activity_Type VALUES(2, 'Exchange');
INSERT INTO Account_Activity_Type VALUES(3, 'Transfer');
INSERT INTO Account_Activity_Type VALUES(4, 'Unload');

INSERT INTO Account_Activity_Group_Type VALUES(1, 'Deposit');
INSERT INTO Account_Activity_Group_Type VALUES(2, 'Exchange');
INSERT INTO Account_Activity_Group_Type VALUES(3, 'Transfer');
INSERT INTO Account_Activity_Group_Type VALUES(4, 'Withdrawal');

INSERT INTO Currency VALUES(978, 'EUR', 2, 'Euro', '€');
INSERT INTO Currency VALUES(840, 'USD', 2, 'US Dollar', '$');
INSERT INTO Currency VALUES(826, 'GBP', 2, 'Pound Sterling', '£');

INSERT INTO Status VALUES(1, 'Active');
INSERT INTO Status VALUES(2, 'Suspended');
INSERT INTO Status values(3, 'Retired');

INSERT INTO User_Role VALUES(1, 'Admin');
INSERT INTO User_Role VALUES(2, 'Normie');

INSERT INTO User VALUES(1, 'admin@cerberus.io', 'Ｒ♢♢Ｔ', '<ｏ.ｏ>【＊】', 1, 1);

INSERT INTO Session VALUES(1, '2018-12-31 00:00:00', 1);
